yolov5.tflite in android

1: app需要系统签名

2：设置persist.sys.save_detect_bitmap yes/no  (默认为空 "")
	将数据源图保存到app文件目录 /data/user/0/com.hra.sittingposturedetect/files/images
	图片命名：bitmap_for_detect_+currentTime+_+result+.jpg
	result包含坐姿文本+confidence，可用Pull出来查看验证识别准确率

3：app可以从桌面点击启动，也可以使用adb shell指令启动：am start-foreground-service com.hra.sittingposturedetect/.CameraService
   关闭APP，只能使用：am force-stop com.hra.sittingposturedetect
   查看进程是否启动：ps -ef | grep sittingposturedetect
   查看当前服务是否启动：dumpsys activity services | grep sittingposturedetect